<?php

/**
 * @file
 * Hooks related to Webform Product module.
 */

// phpcs:disable DrupalPractice.CodeAnalysis.VariableAnalysis.UnusedVariable

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Implements hook_webform_product_configuration_form_alter().
 *
 * This hook enables you to add or modify form elements in the Webform Product Handler
 * configuration form, allowing for additional settings or mappings to be configured
 * when setting up the handler.
 *
 * @param \Drupal\webform_product\Plugin\WebformHandler\WebformProductWebformHandler $handler
 *   The Webform Product Webform Handler being configured.
 * @param array &$form
 *   An associative array containing the structure of the configuration form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the configuration form.
 */
function hook_webform_product_configuration_form_alter(WebformProductWebformHandler $handler, array &$form, FormStateInterface $form_state) {
  // Add additional form elements to the Webform Product Handler configuration form.
  $config = $handler->getConfiguration();
  // Add additional mapping for the billing profile
  // to the webform product handler configuration.
  if (isset($form['contact_mapping'])) {
    $field_types = ['date'];
    $form['contact_mapping']['birthdate'] = [
      '#type' => 'select',
      '#title' => t('Birthday'),
      '#options' => $handler->getElementsSelectOptions($field_types),
      '#default_value' => $config['settings']['birthdate'],
      '#empty_value' => '',
      '#description' => t(
        'Field types allowed: @types.',
        [
          '@types' => implode(', ', $field_types),
        ]
      ),
    ];
  }
}

/**
 * Alter the default configuration of the Webform Product Handler Plugin.
 *
 * @param array $default_configuration
 *   The default configuration of the Webform Product Handler Plugin.
 */
function hook_webform_product_default_configuration_alter(array &$default_configuration) {
  // Add additional default configuration.
  $default_configuration['birthdate'] = NULL;
}

 /**
  * @} End of "addtogroup hooks".
  */
