<?php

namespace Drupal\Tests\webform_product\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\commerce_price\Entity\CurrencyInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Price;
use Drupal\webform_product\Plugin\WebformHandler\WebformProductWebformHandler;
use Drupal\webform_product\Controller\WebformProductController;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @group WebformProduct
 */
class WebformProductWebformHandlerTest extends UnitTestCase {

  /**
   * Create Mocks for the tests.
   */
  protected function runOrderItemsTest($price_fields, $getElement, $submission, $configuration) {
    /** @var \Drupal\webform_product\Plugin\WebformHandler\WebformProductWebformHandler|MockObject $handler */
    $handler = $this->getMockBuilder(WebformProductWebformHandler::class)
    ->disableOriginalConstructor()
    ->onlyMethods([
      'getStore',
      'getWebform',
      'getSavedPaymentStatus',
      'replaceTokens',
      'createOrderItem',
    ])
    ->getMock();

    $store_mock = $this->createMock(StoreInterface::class);

    $currency_mock = $this->createMock(CurrencyInterface::class);

    $currency_mock
    ->method('getCurrencyCode')
    ->willReturn('USD');

    $store_mock
    ->method('getDefaultCurrency')
    ->willReturn($currency_mock);

    $handler
    ->method('getStore')
    ->willReturn($store_mock);

    $webform_mock = $this->createMock(WebformInterface::class);
    $webform_mock
    ->method('getThirdPartySettings')
    ->with('webform_product')
    ->willReturn($price_fields);

    $webform_mock
    ->method('getElement')
    ->willReturnCallback(function ($key) use ($getElement) {
      return $getElement[$key] ?? null;
    });

    $handler
    ->method('getWebform')
    ->willReturn($webform_mock);

    $submission_mock = $this->createMock(WebformSubmissionInterface::class);
    $submission_mock
    ->method('getData')
    ->willReturn($submission);

    $submission_mock
    ->method('getElementData')
    ->willReturnCallback(function ($key) use ($submission) {
        return $submission[$key] ?? null;
    });

    $handler
    ->method('getSavedPaymentStatus')
    ->with($submission_mock)
    ->willReturn(WebformProductController::PAYMENT_STATUS_NULL);

    $handler
    ->method('replaceTokens')
    ->willReturn('My Webform Submission Title');

    $handler
    ->method('createOrderItem')
    ->willReturnCallback(function (array $values) {
      $mockOrderItem = $this->createMock(OrderItemInterface::class);

      $price_number = $values['unit_price']['number'];
      $price_currency = $values['unit_price']['currency_code'];
      $priceObject = new Price($price_number, $price_currency);

      $mockOrderItem->method('getUnitPrice')
        ->willReturn($priceObject);

      $mockOrderItem->method('getQuantity')
        ->willReturn($values['quantity']);

      $mockOrderItem->method('getTitle')
        ->willReturn($values['title'] ?? '');

      return $mockOrderItem;
    });

    $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);

    $reflection = new \ReflectionClass($handler);
    $method = $reflection->getMethod('getOrderItems');
    $method->setAccessible(true);

    $property = $reflection->getProperty('pluginId');
    $property->setAccessible(TRUE);
    $property->setValue($handler, 'webform_product');

    $property = $reflection->getProperty('label');
    $property->setAccessible(TRUE);
    $property->setValue($handler, $configuration['label']);

    $property = $reflection->getProperty('configuration');
    $property->setAccessible(TRUE);
    $property->setValue($handler, $configuration['settings']);

    $property = $reflection->getProperty('eventDispatcher');
    $property->setAccessible(true);
    $property->setValue($handler, $eventDispatcherMock);

    /** @var \Drupal\commerce_order\Entity\OrderItemInterface[] $orderItems */
    return $method->invoke($handler, $submission_mock);
  }

  /**
   * Test Total price option in handler.
   */
  public function testGetOrderItemsTotalPrice() {
    $price_fields = [
      "number" => [
        "top" => "",
      ],
    ];

    $submission = [
      "name" => "test",
      "email" => "test@test.com",
      "order_status" => "",
      "order_id" => "",
      "order_url" => "",
      "number" => "20",
    ];

    $configuration = [
      'label' => 'Webform product',
      'settings' => [
        'order_type' => 'default',
        'order_item_title' => '[webform_submission:source-title]',
        'order_item_type' => 'webform',
        'order_total' => "number",
        'total_price' => '',
      ],
    ];

    $getElement = [
      "number" => [
        "#type" => "number",
        "#title" => "Number",
      ],
    ];

    $orderItems = $this->runOrderItemsTest($price_fields, $getElement, $submission, $configuration);

    $this->assertCount(1, $orderItems, 'Exactly one order item should be created.');
    $this->assertEquals('20', $orderItems[0]->getUnitPrice()->getNumber(), 'The price should be 20.');
    $this->assertEquals('1', $orderItems[0]->getQuantity(), 'The quantity should be 1.');
  }



  /**
   * Test select options.
   */
  public function testGetOrderItemsPriceSelection() {

    $price_fields = [
      'price' => [
        'options' => [
          10 => '10',
          20 => '20',
          30 => '30',
        ],
      ],
      'price2' => [
        'options' => [
          60 => '60',
          70 => '70',
          80 => '80',
        ],
      ],
    ];

    $getElement = [
      'price' => [
        '#type' => 'select',
        '#title' => 'Price',
        '#options' => [
          10 => '10',
          20 => '20',
          30 => '30',
        ],
        '#webform_parent_key' => '',
      ],
      'price2' => [
        '#type' => 'select',
        '#title' => 'Price2',
        '#options' => [
          60 => '60',
          70 => '70',
          80 => '80',
        ],
        '#webform_parent_key' => '',
      ]
    ];

    $configuration = [
      'label' => 'Webform product',
      'settings' => [
        'order_type' => 'default',
        'order_item_title' => '[webform_submission:source-title]',
        'order_item_type' => 'webform',
        'order_total' => '',
        'total_price' => '',
      ],
    ];

    $submission = [
      'name' => 'test',
      'email' => 'test@test.com',
      'price' => '30',
      'price2' => '80',
      'order_status' => '',
      'order_id' => '',
      'order_url' => '',
    ];

    $orderItems = $this->runOrderItemsTest($price_fields, $getElement, $submission, $configuration);

    $this->assertCount(2, $orderItems, 'Exactly two order items should be created.');

    $this->assertEquals('30', $orderItems[0]->getUnitPrice()->getNumber(), 'The price should be 30.');
    $this->assertEquals('1', $orderItems[0]->getQuantity(), 'The quantity should be 1.');
    $this->assertEquals('80', $orderItems[1]->getUnitPrice()->getNumber(), 'The price should be 80.');
    $this->assertEquals('1', $orderItems[1]->getQuantity(), 'The quantity should be 1.');
  }

  /**
   * Test quantity change.
   */
  public function testGetOrderItemsQuantityChange() {

    $price_fields = [
      'price' => [
        'options' => [
          10 => '10',
          20 => '20',
          30 => '30',
        ],
      ],
      'price2' => [
        'options' => [
          60 => '60',
          70 => '70',
          80 => '80',
        ],
      ],
      "number" => [
        "top" => "",
      ],
    ];

    $getElement = [
      'price' => [
        '#type' => 'select',
        '#title' => 'Price',
        '#options' => [
          10 => '10',
          20 => '20',
          30 => '30',
        ],
        '#webform_parent_key' => '',
      ],
      'price2' => [
        '#type' => 'select',
        '#title' => 'Price2',
        '#options' => [
          60 => '60',
          70 => '70',
          80 => '80',
        ],
        '#webform_parent_key' => '',
      ],
      "number" => [
        "#type" => "number",
        "#title" => "Number",
      ],
    ];

    $configuration = [
      'label' => 'Webform product',
      'settings' => [
        'order_type' => 'default',
        'order_item_title' => '[webform_submission:source-title]',
        'order_item_type' => 'webform',
        'order_total' => '',
        'order_quantity' => 'number',
        'total_price' => '',
      ],
    ];

    $submission = [
      'name' => 'test',
      'email' => 'test@test.com',
      'price' => '30',
      'price2' => '80',
      'number' => '2',
      'order_status' => '',
      'order_id' => '',
      'order_url' => '',
    ];

    $orderItems = $this->runOrderItemsTest($price_fields, $getElement, $submission, $configuration);

    $this->assertCount(2, $orderItems, 'Exactly two order items should be created.');

    $this->assertEquals('30', $orderItems[0]->getUnitPrice()->getNumber(), 'The price should be 30.');
    $this->assertEquals('2', $orderItems[0]->getQuantity(), 'The quantity should be 2.');
    $this->assertEquals('80', $orderItems[1]->getUnitPrice()->getNumber(), 'The price should be 80.');
    $this->assertEquals('2', $orderItems[1]->getQuantity(), 'The quantity should be 2.');
  }


  /**
   * Test title options.
   */
  public function testGetOrderItemsTitleOptions() {

    $price_fields = [
      "price" => [
        "options" => [
          10 => "10",
          20 => "20",
          30 => "30",
        ],
      ],
      "checkboxes" => [
        "top" => "",
        "options" => [
          50 => "50",
          100 => "100",
          500 => "500",
          400 => "400",
        ],
      ],
      "number" => [
        "top" => "200",
      ],
    ];

    $getElement = [
      "price" => [
        "#type" => "select",
        "#title" => "Price",
        "#options" => [
          10 => "10",
          20 => "20",
          30 => "30",
        ],
      ],
      "checkboxes" => [
        "#type" => "checkboxes",
        "#title" => "Checkboxes",
        "#options" => [
          50 => "50",
          100 => "100",
          500 => "500",
          400 => "400",
        ],
      ],
      "number" => [
        "#type" => "number",
        "#title" => "Number",
      ],
    ];

    $configuration = [
      'label' => 'Webform product',
      'settings' => [
        'order_type' => 'default',
        'order_item_title' => '[webform_submission:source-title]',
        'order_item_type' => 'webform',
        'order_total' => '',
        'total_price' => '',
      ],
    ];

    $submission = [
      "name" => "test",
      "email" => "test@test.com",
      "price" => "20",
      "order_status" => "",
      "order_id" => "",
      "order_url" => "",
      'number' => '10',
      "checkboxes" => [
        0 => "500",
      ],
    ];

    $orderItems = $this->runOrderItemsTest($price_fields, $getElement, $submission, $configuration);

    $this->assertCount(3, $orderItems, 'Exactly three order items should be created.');

    $this->assertEquals('20', $orderItems[0]->getUnitPrice()->getNumber(), 'The price should be 20.');
    $this->assertEquals('1', $orderItems[0]->getQuantity(), 'The quantity should be 1.');
    $this->assertEquals('My Webform Submission Title - Price', $orderItems[0]->getTitle(), 'Title should be "My Webform Submission Title - Price"');
    $this->assertEquals('200', $orderItems[1]->getUnitPrice()->getNumber(), 'The price should be 200.');
    $this->assertEquals('1', $orderItems[1]->getQuantity(), 'The quantity should be 1.');
    $this->assertEquals('My Webform Submission Title - Number', $orderItems[1]->getTitle(), 'Title should be "My Webform Submission Title - Number"');
    $this->assertEquals('500', $orderItems[2]->getUnitPrice()->getNumber(), 'The price should be 500.');
    $this->assertEquals('1', $orderItems[2]->getQuantity(), 'The quantity should be 1.');
    $this->assertEquals('My Webform Submission Title - Checkboxes - 500', $orderItems[2]->getTitle(), 'Title should be "My Webform Submission Title - Checkboxes - 500"');


    $configuration['settings']['order_item_title_only'] = TRUE;
    $orderItems = $this->runOrderItemsTest($price_fields, $getElement, $submission, $configuration);

    $this->assertEquals('My Webform Submission Title', $orderItems[0]->getTitle(), 'Title should be "My Webform Submission Title"');
    $this->assertEquals('My Webform Submission Title', $orderItems[1]->getTitle(), 'Title should be "My Webform Submission Title"');
    $this->assertEquals('My Webform Submission Title', $orderItems[2]->getTitle(), 'Title should be "My Webform Submission Title"');

  }

}
