<?php

namespace Drupal\webform_product\Event;

use Drupal\commerce_order\Entity\Order;
use Drupal\Component\EventDispatcher\Event;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Class OrderEvent.
 *
 * Provides an event to let other modules alter the attached order.
 *
 * @package Drupal\webform_product\Event
 */
class OrderEvent extends Event {

  const EVENT_NAME = 'webform_product_order';

  /**
   * The webform submission.
   *
   * @var \Drupal\webform\WebformSubmissionInterface
   */
  public $webformSubmission;

  /**
   * The commerce_order entity attached to the webform submission.
   *
   * @var \Drupal\commerce_order\Entity\Order
   */
  public $order;

  /**
   * The webform_product configuration.
   *
   * @var array
   */
  public $configuration;

  /**
   * OrderEvent constructor.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webformSubmission
   *   The webform submission.
   * @param Drupal\commerce_order\Entity\Order $order
   *   The attached order.
   * @param array $configuration
   *   The webform_product configuration.
   */
  public function __construct(WebformSubmissionInterface $webformSubmission, Order $order, array $configuration) {
    $this->webformSubmission = $webformSubmission;
    $this->order = $order;
    $this->configuration = $configuration;
  }

}
