<?php

namespace Drupal\webform_product\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Class ProfileEvent.
 *
 * Provides an event to let other modules alter the profile.
 *
 * @package Drupal\webform_product\Event
 */
class ProfileEvent extends Event {

  const EVENT_NAME = 'webform_product_profile';

  /**
   * The webform submission.
   *
   * @var \Drupal\webform\WebformSubmissionInterface
   */
  public $webformSubmission;

  /**
   * The profile.
   *
   * @var \Drupal\profile\Entity\ProfileInterface
   */
  public $profile;

  /**
   * The webform_product configuration.
   *
   * @var array
   */
  public $configuration;

  /**
   * ProfileEvent constructor.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webformSubmission
   *   The webform submission.
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile.
   * @param array $configuration
   *   The webform_product configuration.
   */
  public function __construct(WebformSubmissionInterface $webformSubmission, ProfileInterface $profile, array $configuration) {
    $this->webformSubmission = $webformSubmission;
    $this->profile = $profile;
    $this->configuration = $configuration;
  }

}
